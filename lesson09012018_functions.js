// task1
// Создайте конструктор function Card(from, to){…}, создающий объекты карточки со свойствами from, to и методом show(), выводящим свойство отдельного объекта.
// Потом выполните задачу используя классы
function Card(from, to){
    this.from = from;
    this.to = to;
    this.show = function(){
        console.log('From: ' + this.from + ' To: ' + this.to)
    };
}

let card = new Card('ten', 'queen')
card.show()

//use class
class Card {
    constructor(from, to){
        this.from = from;
        this.to = to;
    }

    show(){
        console.log('From: ' + this.from + ' To: ' + this.to) 
    }
}

let card = new Card('six', 'nine')
card.show()

// task2
// Создайте конструктор function Card(option){…}, создающий объекты карточки со свойствами from, to и методом show(), выводящим свойство отдельного объекта.
// Примечание: option — объект со свойствами from и to, если в объекте нет свойства to, тогда в конструкторе Сard вместо него должно подставляться любое 
// значение по-молчанию.  Потом выполните задачу используя классы
function Card(option){
    this.from = option.from;
    this.to = option.to || 'defaultValue';
    this.show = function(){
            console.log('From: ' + this.from + ' To: ' + this.to);
    };
}

// let kard = new Card({from: 'five', to: 'king'});
let kard = new Card({from: 'five'});
kard.show();

//use class
class Card{
    constructor(option){
        this.from = option.from;
        this.to = option.to || 'defaultValue';
    }
}

// let kard1 = new Card({from: 'eight'});
let kard1 = new Card({from: 'eight', to: 'ace'});
console.log(kard1)

// task3
// Создайте конструктор function Human(){…}, который создает объекты со свойствами name, age и height. Создайте массив humans из десяти объектов.
// Потом выполните задачу используя классы
function Human(name, age, height){
    this.name = name;
    this.age = age;
    this.height = height;
}
var humans = [];
for(var i = 0; i < 10; i++){
    humans.push(new Human('Name', Math.ceil(Math.random() * 100), Math.ceil(Math.random() * 200)));
}
console.log(humans)

//use class
class Human {
    constructor(name, age, height){
        this.name = name;
        this.age = age;
        this.height = height;
    }
}

var humans = [];
for(var i = 0; i < 10; i++){
    humans.push(new Human('Name', Math.ceil(Math.random() * 100), Math.ceil(Math.random() * 200)));
}
console.log(humans)

// task4
// Отсортируйте массив humans из предыдущего задания по каждому свойству отдельно
var humansCopy = humans.slice().sort(function(a, b){
    return a.age > b.age;
});
console.log(humansCopy)

var humansCopy = humans.slice().sort(function(a, b){
    return a.height > b.height;
});
console.log(humansCopy)

// task5
// Расширьте свойства и методы любых объектов через прототип конструктора. Потом расширьте методы через наследование с классами


// task6
// Получите текущую дату при помощи конструктора Date()
var justNow = new Date();
console.log(justNow)

// task7
// Найдите кол-во секунд, которое прошло с 1 января 1970 года по текущий момент
var currentDate = new Date();
var spendSeconds = currentDate.getTime();
console.log(spendSeconds / 1000 + ' seconds left from 01.01.1970 till now')

// task8
// Напишите функцию, которая принимает номер месяца и год, а возвращает количество дней в месяце
function NumberOfDaysInMonthReturner(k, l){
    return (new Date(k, l, 0)).getDate();
}

console.log(NumberOfDaysInMonthReturner(2016, 2))

// task9
// Через прототип расширьте встроенный объект Number методом isOdd(), который возвращает true, если число нечетное
Number.prototype.isOdd = function(a){
    if (a % 2 != 0) {
        return true;
    } else {
        return false;
    }
}

console.log(new Number().isOdd(3))
console.log(new Number().isOdd(2))

// another solve
var number = new Number();
 
Number.prototype.isOdd = function(a){
    if (a % 2 != 0) {
        return true;
    } else {
        return false;
    }
}
 
console.log(number.isOdd(4))
console.log(number.isOdd(9))

// another2 solve
Number.prototype.isOdd = function(a){
        return (a % 2 != 0);
}

console.log(new Number().isOdd(7))
console.log(new Number().isOdd(6))

// task10
// Создайте конструктор, который поддерживает на своих методах цепочечный синтаксис


// task11
// Представьте, что разрабатываете игру. Создайте конструктор Unit(), создающий объекты с координатами x, y. При помощи 
// прототипного наследования расширьте Unit(), создав новый конструктор Fighter() с свойством power. Примечание: не забудьте о геттерах и сеттерах.
class Unit{
    constructor(x, y){
        this.x = x;
    this.y = y;
    }
}

class Fighter extends Unit{
    constructor(power){
        super(x, y);
        this.power = power;
    }

    get power(){
        return this.power;
    }
    set power(){
        this.power = power;
    }
}
