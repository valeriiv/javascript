// task1
// Сделайте функцию isNumberInRange, которая параметром принимает число и проверяет, что оно больше 
// нуля и меньше 10. Если это так - пусть функция возвращает true, если не так - false
function isNumberInRange(a){
if(a > 0 && a < 10){
return true
}
else
return false
}

console.log(isNumberInRange(6))
console.log(isNumberInRange(0))
console.log(isNumberInRange(-6))

// task2
// Дан массив с числами. Запишите в новый массив только те числа, которые больше нуля и меньше 10-ти.
// Для этого используйте вспомогательную функцию isNumberInRange из предыдущей задачи
var arr = [9,11,3,15,8,17,6,2]
var newArr = []
for(var i = 0; i < arr.length; i++){
    if(isNumberInRange(arr[i]))
        newArr.push(arr[i])
}

console.log(newArr)

// task3
// Сделайте функцию getDigitsSum (digit - это цифра), которая параметром принимает целое число и
// возвращает сумму его цифр
function getDigitsSum(x){
    var x = String(x)
    var summTask3 = 0
    for(var i = 0; i < x.length; i++){
//if use only x[i] then in result we get summ with 0: 06; 016 etc
        summTask3 += Number(x[i])
    }
    return summTask3
}

console.log(getDigitsSum(617))

// task4
// Найдите все года от 1 до 2018, сумма цифр которых равна 13. Для этого используйте вспомогательную
// функцию getDigitsSum из предыдущей задачи
for(var i = 1; i < 2018; i++){
    if(getDigitsSum(i) == 13)
    console.log(i)
}

// task5
// Сделайте функцию isEven() (even - это четный), которая параметром принимает целое число и проверяет:
// четное оно или нет. Если четное - пусть функция возвращает true, если нечетное - false
function isEven(a){
    if(a % 2 == 0) return true
    else return false
}

console.log(isEven(3))
console.log(isEven(4))

// task6
// Дан массив с целыми числами. Создайте из него новый массив, где останутся лежать только четные из этих
// чисел. Для этого используйте вспомогательную функцию isEven из предыдущей задачи
var task6 = [9,11,3,15,8,17,6,2]
var newArr1 = []
for(var i = 0; i < task6.length; i++){
    if(isEven(task6[i]))
    newArr1.push(task6[i])
}

console.log(newArr1)

// task7
// Сделайте функцию getDivisors, которая параметром принимает число и возвращает массив его делителей
// (чисел, на которое делится данное число)
function getDivisors(a){
    var arr = []
    for(var i = 1; i <= a; i++){
        if(a % i == 0)
        arr.push(i)
    }
    return arr
}

console.log(getDivisors(37))