// task2
// С помощью цикла for сформируйте строку '123456789' и запишите ее в переменную str
var str = ''
for(var i = 1; i < 10; i++){
    str += i
}
console.log(str)

//another solve
var str = 0
for(var i = 1; i < 10; i++){
    str += i + ''
}
var fStr = str.substr(1)
console.log(fStr)

// task3
// С помощью цикла for сформируйте строку '987654321' и запишите ее в переменную str
var res = 0
for(var y = 9; y > 0; y--) res += y + ''
var fRes = res.substr(1)
console.log(fRes)

// task4
// С помощью цикла for сформируйте строку '-1-2-3-4-5-6-7-8-9-' и запишите ее в переменную str
var res = '-'
for(var i = 1; i < 10; i++) res+= i + '-'
console.log(res)

// task5
// Нарисуйте пирамиду, как показано на рисунке, только у вашей пирамиды должно быть 20 рядов, а не 5
// x
// xx
// xxx
// xxxx
// xxxxx
var str = ''
for(var i = 1; i < 21; i++){
    str+= i + ''
    console.log(str)
}

// task6
// С помощью двух вложенных циклов нарисуйте следующую пирамидку
// 1
// 22
// 333
// 4444
// 55555
// 666666
// 7777777
// 88888888
// 999999999
for(var x = 1; x < 10; x++){
    for(var y = 0; y < x; y++){
        console.log(x)
    }
    console.log('')
}

// task7
