// task1
function task1(){
    var res = document.getElementById("p_task1");
    res.innerHTML = parseInt(res.innerHTML)+1;
}
function task1_timer(){
    window.setInterval(task1, 1000);
}
//or alternative
function task1_t(){
    window.setInterval(function task1(){
        var res = document.getElementById("p_task1");
        res.innerHTML = parseInt(res.innerHTML)+1;
    }, 1000);
}

let si;
// task2
function task2_start(){
//if you try to set let or var - it's not works, BUT if empty or window then it works
//because we need GLOBAL VARIABLE
    si = setInterval(function task2(){
        let res = document.getElementById("p_task2");
        res.innerHTML = parseInt(res.innerHTML) + 1;
        document.getElementById("start").disabled = true;
        document.getElementById("stop").disabled = false;
    }, 1000);
}
function task2_stop(){
    clearInterval(si);
    document.getElementById("start").disabled = false;
    document.getElementById("stop").disabled = true;
}

// task3
function task3(){
    setInterval(function x(){
        var res = document.getElementById("p_task3");
        var date = new Date();
        res.innerHTML = changeNumbers(date.getHours()) + ':' + changeNumbers(date.getMinutes()) +
         ':' + changeNumbers(date.getSeconds());
    }, 500);
}
//creates dynamic to follows number by number
function changeNumbers(number){
    if (number <= 9) {
        return '0' + number;
    } else {
        return number;
    }
}

// task4
function task4(){
    tim = setInterval(function x(){
        var res = document.getElementById("p_task4");
        var num = res.innerHTML = parseInt(res.innerHTML) - 1;
        res.innerHTML = num;
        if(num == 0){
            task4_finish();
        var res1 = document.getElementById("finish");
        res1.innerHTML = 'Final countdown was finished';
        }
    }, 500);
    document.getElementById("but_task4").disabled = true;
}
function task4_finish(){
    clearInterval(tim);
}

let g_number;
//task5
function task5(){
    setInterval(function x(){
//if set numbers without window - script woun't work
        if(g_number == undefined || g_number == 3) g_number = 1;
        else g_number += 1;
            var im = document.getElementById("img");
            im.src = g_number + '.png';
    }, 1000);
}

// task6
function task6(){
    setInterval(function x(){
        var im1 = document.getElementById("img1");
        var im2 = document.getElementById("img2");
        var im3 = document.getElementById("img3");
        var temp = im1.src;
        im1.src = im2.src;
        im2.src = im3.src;
        im3.src = temp;
    }, 1000);
}

// task7
function task7_start(){
    ti = setInterval(function x(){
        var im1 = document.getElementById("img1");
        var im2 = document.getElementById("img2");
        var im3 = document.getElementById("img3");
        var im4 = document.getElementById("img4");
        var im5 = document.getElementById("img5");
        var im6 = document.getElementById("img6");
        var temp = im1.src;
        im1.src = im2.src;
        im2.src = im3.src;
        im3.src = im4.src;
        im4.src = im5.src;
        im5.src = im6.src;
        im6.src = temp;
    }, 1000);
    document.getElementById("start").disabled = true;
    document.getElementById("finish").disabled = false;
}
function task7_finish(){
    clearInterval(ti);
    document.getElementById("start").disabled = false;
    document.getElementById("finish").disabled = true;
}

//task8
function task8(){
    setInterval(function y(){
var date = new Date();
    var h = document.getElementById("hours");
    var m = document.getElementById("minutes");
    var s = document.getElementById("seconds");
    var midnight = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1, 0, 0, 0);
//transfer into seconds
    var diff = Math.floor((midnight - date) / 1000);
    var hoursRemain = Math.floor(diff / (60 * 60));
    var minutesRemain = Math.floor((diff - hoursRemain * 60 * 60) / 60);
    var secondsRemain = Math.floor(diff % 60);
    h.innerHTML = hoursRemain;
    m.innerHTML = changeNumbers(minutesRemain);
    s.innerHTML = changeNumbers(secondsRemain);
    }, 1000);
}