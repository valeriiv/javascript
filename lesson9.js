// task1
// Дана строка. Сделайте заглавным первый символ этой строки не используя цикл. Найдите два решения
//first solve
var str = 'afdjflgghfk';
var first = str.charAt(0).toUpperCase();
var residue = first + str.substr(1);
console.log(residue);
//second solve
var str = 'afdjflgghfk';
var firstUp = str.toUpperCase().substr(0, 1);
var result = firstUp + str.substr(1);
console.log(result);
//third solve
var str = 'afdjflgghfk';
str = str.split('');
str[0] = str[0].toUpperCase();
var result = str.join('');
console.log(result);

// task2
// Дана строка, например, '123456'. Переверните эту строку (сделайте из нее '654321') не используя цикл
var str2 = '123456'
str2 = str2.split('').reverse()
var res = str2.join('')
console.log(res)

// task3
// Дано число, например, 3751. Отсортируйте цифры в нем (сделайте из него 1357) не используя цикл
var num = 3751
var sNum = num + ''
var ar = sNum.split('')
ar.pop()
ar.splice(0, 0, '1')
ar.splice(2, 2)
ar.splice(2, 0, '5', '7')
var res = ar.join('')
console.log(res)
//more elegant solve if original number IS A STRING huh :)
var num = '3751'
var ar = num.split('')
ar.sort()
console.log(ar)

// task4
// Проверьте, что строка начинается на http://
// var someStr = 'whatever this string consist of'
var someStr = 'http://phphtml'
if (someStr.startsWith('http://')) {
    console.log('The string starts with http://')
} else {
    console.log('The string doesn\'t starts with http://')
}
//another solve more ugly as I think
// var someStr = 'whatever this string consist of'
var someStr = 'http://phphtml'
if (someStr.substr(0, 7) == 'http://') {
    console.log('The string starts with http://')
} else {
    console.log('The string doesn\'t starts with http://')
}

// task5
// Проверьте, что строка заканчивается на .html
var task5 = 'htmlpage.html'
if (task5.endsWith('.html')) {
    console.log('The string ends with .html')
} else {
    console.log('The string doesn\'t ends with .html')
}

//another solve with substr function use
var task5 = 'htmlpage.html'
if (task5.substr(-5) == '.html') {
    console.log('The string ends with .html')
} else {
    console.log('The string doesn\'t ends with .html')
}