// task1
// Даны картинки. Привяжите к каждой картинке событие, чтобы по клику на картинку 
// алертом выводился ее src.
// <img src="img/1.jpg">
// <img src="img/2.jpg">
// <img src="img/3.jpg">
var els = document.getElementsByTagName("img");
for(var i = 0; i < els.length; i++)
els[i].onclick = task1;

function task1(){
    alert(this.getAttribute("src"));
}

// task2
// Даны ссылки. Привяжите всем ссылкам событие - по наведению на ссылку в атрибут
// title запишется ее текст.
var els = document.getElementsByTagName("a");
for(var j=0;j<els.length;j++)
els[j].addEventListener('mouseout', task2);

function task2(){
    this.title = this.innerHTML;
}

// task3
// Привяжите всем ссылкам событие - по наведению на ссылку в конец ее текста дописывается
// ее href в круглых скобках.
// var els = document.getElementsByTagName("a");
// for(var y = 0; y < els.length; y++)
// els[y].addEventListener('mouseover', task3);

// function task3(){
//     this.innerHTML = this.innerHTML + ' (' + this.href + ')';
// }

// task4
// Дополните предыдущую задачу: после первого наведению на ссылку следует отвязать от нее
// событие, которое добавляет href в конец текста.
var els = document.getElementsByTagName("a");
for(var y = 0; y < els.length; y++)
els[y].addEventListener('mouseover', task4);

function task4(){
    this.innerHTML = this.innerHTML + ' (' + this.href + ')';
    this.removeEventListener('mouseover', task4); 
}

// task5
// Привяжите всем инпутам следующее событие - по потери фокуса
// каждый инпут выводит свое value в абзац с id="test".
var res = document.getElementsByTagName("input");
var resP = document.getElementById("test");
for(var z = 0; z < res.length; z++)
res[z].addEventListener('blur', task5);

function task5(){
    resP.innerHTML = this.value;
}

// task6
// Для всех инпутов сделайте так, чтобы они выводили свой
// value алертом при нажатии на любой из них, но только по первому
// нажатию. Повторное нажатие на инпут не должно вызывать реакции.
var els = document.getElementsByTagName("input");
for(var w = 0; w < els.length; w++)
els[w].addEventListener('click', task6);

function task6(){
    alert(this.value);
    this.removeEventListener('click', task6);
}

// task7
// Даны абзацы с числами. По нажатию на абзац в нем
// должен появится квадрат числа, которое он содержит.
var res = document.getElementsByClassName("numberial");
for(var v = 0; v < res.length; v++)
res[v].addEventListener('click', task7);

function task7(){
    this.innerHTML = this.innerHTML * this.innerHTML;
//set ONLY ONE calculate
    this.removeEventListener('click', task7);
}

// task8
// Даны инпуты. Сделайте так, чтобы все инпуты по потери
// фокуса проверяли свое содержимое на правильное количество
// символов. Сколько символов должно быть в инпуте, указывается
// в атрибуте data-length. Если вбито правильное количество, то
// граница инпута становится зеленой, если неправильное - красной.
var res = document.getElementsByTagName("input");
for(var b = 0; b < res.length; b++)
res[b].addEventListener('blur', task8);

function task8(){
    var corr = this.dataset.length;
    var curr = this.value.length;
    if (curr == corr) {
        this.style.borderColor = 'green';
    } else {
        this.style.borderColor = 'red';
    } 
}

// task9
// Даны дивы. По первому нажатию на каждый див он красится красным
// фоном, по второму красится обратно и так далее каждый клик происходит
// чередование фона. Сделайте так, чтобы было две функции: одна красит в 
// красный цвет, другая в зеленый и они сменяли друг друга через removeEventListener.
var res = document.getElementsByTagName("div");
for(var i = 0; i < res.length; i++)
res[i].addEventListener('click', task9Red);

function task9Red(){
    this.style.background = 'red';
    this.removeEventListener('click', task9Red);
    this.addEventListener('click', task9Green);
}
function task9Green(){
    this.style.background = 'green';
    this.removeEventListener('click', task9Gren);
    this.addEventListener('click', task9Red);
}