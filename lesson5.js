// task1
// Выведите столбец чисел от 1 до 100
var i = 1;
while(i <= 100){
    console.log(i);
    i++;
}

for(var i = 1; i <= 100; i++) {
    console.log(i);
}

for(var i = 1; i <= 100;) {
    console.log(i);
    i++;
}

// task2
//  Выведите столбец чисел от 11 до 33
var i = 11;
while(i <= 33){
    console.log(i);
    i++;
}

for(var i = 11; i <= 33; i++) {
    console.log(i);
}

// task3
//  Выведите столбец четных чисел в промежутке от 0 до 100
var i = 0;
while(i <= 100){
    console.log(i);
    i+=2;
}

for(var i = 0; i <= 100; i+=2) {
    console.log(i);
}

// task4
//  С помощью цикла найдите сумму чисел от 1 до 100
var i = 1;
var summ = 0;
while(i <= 100){
    summ+=i;
    i++;
}
console.log(summ);

summ = 0;
for(var i = 1; i <= 100; i++) {
    summ+=i;
}
console.log(summ);

// task5
// Дан массив с элементами [1, 2, 3, 4, 5]. С помощью цикла for выведите все эти элементы на экран
var arr = [1, 2, 3, 4, 5];
for(var y = 0; y < arr.length; y++) console.log(arr[y]);

// task6
// Дан массив с элементами [1, 2, 3, 4, 5]. С помощью цикла for найдите сумму элементов этого массива.
// Запишите ее в переменную result
var arr2 = [1, 2, 3, 4, 5];
var summ = 0;
for(var y = 0; y < arr2.length; y++) summ+= arr2[y];
console.log(summ);

// task7
// Дан объект obj. С помощью цикла for-in выведите на экран ключи и элементы этого объекта
var obj = {green: 'зеленый', red: 'красный', blue: 'голубой'};
for(var keys in obj) {
console.log("Keys " + keys);
console.log("Values " + obj[keys]);
}

// task8
// Дан объект obj с ключами Коля, Вася, Петя с элементами '200', '300', '400'. С помощью цикла
// for-in выведите на экран строки такого формата: 'Коля - зарплата 200 долларов.'
var obj = {Коля:200, Вася:300, Петя:400};
for (var key in obj) {
    console.log(key + ' ' + obj[key] + ' долларов.')
}

// task9
// Дан массив с элементами 2, 5, 9, 15, 0, 4. С помощью цикла for и оператора if выведите на экран
// столбец тех элементов массива, которые больше 3-х, но меньше 10
var arr3 = [2, 5, 9, 15, 0, 4];
for(var x = 0; x < arr3.length; x++){
    if((arr3[x] > 3) && (arr3[x] < 10)) console.log(arr3[x]);
}

// task10
// Дан массив с числами. Числа могут быть положительными и отрицательными. Найдите сумму положительных элементов массива
var arr4 = [-2, -5, 9, 15, 0, -4];
var summ = 0;
for(var x = 0; x < arr4.length; x++){
    if(arr4[x] > 0) summ += arr4[x];   
}
console.log(summ);

// task11
// Дан массив с элементами 1, 2, 5, 9, 4, 13, 4, 10. С помощью цикла for и оператора if проверьте есть ли в массиве элемент
// со значением, равным 4. Если есть - выведите на экран 'Есть!' и выйдите из цикла. Если нет - ничего делать не надо
var arr4 = [1, 2, 5, 9, 4, 13, 4, 10];
for(var iu = 0; iu < arr4.length; iu++){
    if(arr4[iu] == 4) console.log(arr4[iu]);
}

// task12
// Дан массив числами, например: [10, 20, 30, 50, 235, 3000]. Выведите на экран только те числа из массива, которые начинаются
// на цифру 1, 2 или 5
var arr5 = [10, 20, 30, 50, 235, 3000];
var interm;
for(var i = 0; i < arr5.length; i++){
interm = (arr5[i] + '');
  if((new Number(interm.charAt(0)) == 1) || (new Number(interm.charAt(0)) == 2) || (new Number(interm.charAt(0)) == 5)) console.log(arr5[i]);
}

//alternative
var arr5 = [10, 20, 30, 50, 235, 3000];
for(var i = 0; i < arr5.length; i++){
interm = (arr5[i] + '');
    if((parseInt(interm.charAt(0)) == 1) || (parseInt(interm.charAt(0)) == 2) || (parseInt(interm.charAt(0)) == 5)) console.log(arr5[i]);
}

//task13
// Дан массив с элементами 1, 2, 3, 4, 5, 6, 7, 8, 9. С помощью цикла for создайте строку '-1-2-3-4-5-6-7-8-9-'
var arr6 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var res = '-';
for(var i = 0; i < arr6.length; i++){
res += arr6[i] + '-';
}
console.log(res);

// task14
// Составьте массив дней недели. С помощью цикла for выведите все дни недели, а выходные дни выведите жирным
var dayArr = ['mon', 'tue', 'wen', 'thu', 'fri', 'sat', 'sun'];
for(var z = 0; z < dayArr.length; z++){
    if (dayArr[z] != 'sat' || dayArr[z] != 'sun') {
        document.write(dayArr[z] + '<br>')
    } else {
        document.write('<b>' + dayArr[z] + '</b>' + '<br>')
    }
}

// task15
// Составьте массив дней недели. С помощью цикла for выведите все дни недели, а текущий день выведите курсивом.
// Текущий день должен храниться в переменной day
var dayArr = ['mon', 'tue', 'wen', 'thu', 'fri', 'sat', 'sun'];
var date = new Date();
for(var z = 0; z < dayArr.length; z++){
    if (dayArr[z] == date.getDay) {
        document.write('<i>' + dayArr[z] + '</i>')
    }
    console.log(dayArr[z]);
}

// task16
// Дано число n=1000. Делите его на 2 столько раз, пока результат деления не станет меньше 50. Какое число получится?
// Посчитайте количество итераций, необходимых для этого (итерация - это проход цикла), и запишите его в переменную num
var n = 1000;
var num = 0;
while(n > 50){
    n /= 2;
    num++;
}
console.log('Result is: ' + n);
console.log('Numbers of iterations: ' + num);