// task1
// Даны два массива: [1, 2, 3] и [4, 5, 6]. Объедините их вместе
var arr1 = [1, 2, 3];
var arr2 = [4, 5, 6];
//first possible solve
var c = arr1.concat(arr2);
console.log(c);
//second possible solve
var c = arr2.concat(arr1);
console.log(c);

// task2
// Дан массив [1, 2, 3]. Сделайте из него массив [3, 2, 1]
var arr3 = [1, 2, 3];
var rev = arr3.reverse();
console.log(rev);

// task3
// Дан массив [1, 2, 3]. Добавьте ему в конец элементы 4, 5, 6
var arr4 = [1, 2, 3];
arr4.push(4,5,6);
console.log(arr4);

// task4
// Дан массив [1, 2, 3]. Добавьте ему в начало элементы 4, 5, 6
var arr5 = [1, 2, 3];
arr5.unshift(4,5,6);
console.log(arr5);

// task5
// Дан массив ['js', 'css', 'jq']. Выведите на экран первый элемент
var nArr = ['js', 'css', 'jq'];
console.log(nArr[0]);
//OR
console.log(nArr.shift());

// task6
// Дан массив ['js', 'css', 'jq']. Выведите на экран последний элемент
var nArr1 = ['js', 'css', 'jq'];
console.log(nArr1[nArr1.length-1]);
//OR
console.log(nArr1.pop());

// task7
// Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [1, 2, 3]
var arr6 = [1, 2, 3, 4, 5];

// task8
// Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [4, 5]
var arr6 = [1, 2, 3, 4, 5];
var anotherArr = arr6.slice(arr6.length - 2);
console.log(anotherArr);

// task9
// Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 4, 5]
var arr7 = [1, 2, 3, 4, 5];
var anotherArr1 = arr7.splice(0, 1);
var anotherArr2 = arr7.splice(2);
var finRes = anotherArr1.concat(anotherArr2);
console.log(finRes);
//alternative
var arr7 = [1, 2, 3, 4, 5]
arr7.splice(1, 2)
console.log(arr7)

// task10
// Дан массив [1, 2, 3, 4, 5]. С помощью метода splice запишите в новый массив элементы [2, 3, 4]
var arr8 = [1, 2, 3, 4, 5];
var res = arr8.splice(1, 3);
console.log(res);

// task11
// Дан массив [1, 2, 3, 4, 5]. С помощью метода splice сделайте из него массив [1, 2, 3, 'a', 'b', 'c', 4, 5]
var arr9 = [1, 2, 3, 4, 5];
arr9.splice(3, 0, 'a', 'b', 'c');
console.log(arr9);

// task12
// Дан массив [1, 2, 3, 4, 5]. С помощью метода splice сделайте из него массив [1, 'a', 'b', 2, 3, 4, 'c', 5, 'e']
var arr10 = [1, 2, 3, 4, 5]
arr10.splice(1, 0, 'a', 'b')
arr10.splice(6, 0, 'c')
arr10.push('e')
console.log(arr10)

// task13
// Дан массив [3, 4, 1, 2, 7]. Отсортируйте его
var arr13 = [3, 4, 1, 2, 7];
//ASC sorting
arr13.sort(function(x, y){return x > y});
console.log(arr13);
//DESC sorting
arr13.sort(function(x, y){return x < y});
console.log(arr13);

// task14
// Дан объект {js:'test', jq: 'hello', css: 'world'}. Получите массив его ключей
var obj = {js:'test', jq: 'hello', css: 'world'};
console.log(Object.keys(obj));