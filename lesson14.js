// task1
// Заполните массив следующим образом: в первый элемент запишите 'x', во второй 'xx',
// в третий 'xxx' и так далее
var arr = [];
var filler = 'x';
var adding = 'x';
for(var y = 0; y < 100; y++){
    arr.push(filler);
    filler += adding;
}
console.log(arr);

// task2
// Заполните массив следующим образом: в первый элемент запишите '1', во второй '22',
// в третий '333' и так далее
var arr1 = [];
for(var x = 0; x < 100; x++){
    var number = ''
    for(var y = 0; y < x; y++){
        number += x
    }
    arr1.push(number)
}
console.log(arr1)

// task3
// Сделайте функцию arrayFill, которая будет заполнять массив заданными значениями.
// Первым параметром функция принимает значение, которым заполнять массив, а вторым
// - сколько элементов должно быть в массиве. Пример: arrayFill('x', 5) сделает массив
// ['x', 'x', 'x', 'x', 'x']
function arrayFill(a, b){
    var arr2 = []
    for(var i = 0; i < b; i++){
        arr2.push(a)
    }
    return arr2
}
console.log(arrayFill('z', 8))

// task4
// Дан массив с числами. Узнайте сколько элементов с начала массива надо сложить, чтобы в 
// сумме получилось больше 10-ти
function ls14ts4(arr3){
    var summ = 0
    for(var x = 0; x < arr3.length; x++){
        summ += arr3[x]
        if(summ > 10)
        return x + 1 //because the first element is 0
    }
}

console.log(ls14ts4([9,1,3,5,8,7,6,2]))

// task5
// Дан массив с числами. Не используя метода reverse переверните его элементы в обратном порядке
//bubble sort
function rev(arr4){
    for(var i = 0; i < arr4.length; i++)
        for(var j = 0; j < i; j++){
            if(arr4[j] < arr4[j+1]){
                var temp = arr4[j]
                arr4[j] = arr4[j+1]
                arr4[j+1] = temp
            }
        }
        return arr4
}

console.log(rev([9,1,3,5,8,7,6,2]))

function revv(arr4){
    var reverseArr4 = []
    for(var i = arr4.length - 1; i >= 0; i--)
        reverseArr4.push(arr4[i])
        return reverseArr4
}

console.log(revv([9,1,3,5,8,7,6,2]))

// task6
// Дан двухмерный массив с числами, например 
// [[1, 2, 3], [4, 5], [6]]. Найдите сумму элементов этого массива. Массив, конечно же, может 
// быть произвольным
var twoDimensionalArray = [[1, 2, 3], [4, 5], [6]]
var summTDA = 0
for(var x = 0; x < twoDimensionalArray.length; x++){
    for(var y = 0; y < twoDimensionalArray[x].length; y++)
    summTDA += twoDimensionalArray[x][y]
}

console.log(summTDA)

// task7
// Дан трехмерный массив с числами, например [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]. Найдите 
// сумму элементов этого массива. Массив, конечно же, может быть произвольным
var task7 = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]
var summ = 0
for (var i = 0; i < task7.length; i++) {
	for (var j = 0; j < task7[i].length; j++) {
  		for (var k = 0; k < task7[i][j].length; k++) {
			summ += task7[i][j][k]
   		}
	}
}
console.log(summ)