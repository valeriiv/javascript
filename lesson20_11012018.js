// task1
function task1(){
    var res = document.getElementById("task1");
    res.innerHTML = 'I was changed! <b>bold!</b>';
}

// task2
function task2(){
    var res = document.getElementById("task2");
    res.outerHTML = '<h3> ' + 'p was changed into h3' + '</h3>';
}

// task3
function task3(){
    var res = document.getElementById("task3");
    res.outerHTML = '<h3> ' + res.innerHTML + ' </h3>';
}

// task4
function task4(){
    // var res = document.getElementById("inp1").value;
    // var res1 = document.getElementById("inp2").value;
    // var fin = document.getElementById("sp");
    // fin.innerHTML = +res + +res1;
    //alternative
    var res = +document.getElementById("inp1").value;
    var res1 = +document.getElementById("inp2").value;
    var fin = document.getElementById("sp");
    fin.innerHTML = res + res1;
}

// task5
function task5(){
    var res = document.getElementsByTagName("p");
    for(var i = 0; i < res.length; i++)
    res[i].innerHTML = 'Just something else!';
}

// task6
function task6(){
    var res = document.getElementsByClassName("www");
    for(var i = 0; i < res.length; i++)
    res[i].innerHTML = i + 1;
}

// task7
function task7(){
    var res = document.querySelectorAll("p");
    for(var i = 0; i < res.length; i++)
    res[i].innerHTML = i + 1;
}

// task8
function task8(){
    var res = document.getElementById("task8");
    alert(res.getAttribute('class'));
}

// task1_12012018
//дописать функцию parallel
function parallel(funcArray, doneAll) {
//funcArray[0](resolve) needed because function(done)
  let p1 = new Promise( (resolve, rejected) => {funcArray[0](resolve)});
  let p2 = new Promise( (resolve, rejected) => {funcArray[1](resolve)});
  Promise.all([p1, p2]).then(values => doneAll(values));
}
 
var a = function(done) {
  setTimeout(function() {
    done('result a');
  }, 300);
};
 
var b = function(done) {
  setTimeout(function() {
    done('result b');
  }, 200);
};
 
parallel([a,b], function(results) {
  console.log(results); // ['result a', 'result b']
});

// task2_12012018
function all(promises) {
    return new Promise(function(success, fail) {
        Promise.all( promises )
        .then(task2_12012018 => success(task2_12012018))
        .catch(error => fail(error));
    });
}
  
// Test code.
all([]).then(function(array) {
    console.log("This should be []:", array);
});

function soon(val) {
    return new Promise(function(success) {
        setTimeout(
            function() { success(val); },
            Math.random() * 500
        );
    });
}

all([soon(1), soon(2), soon(3)]).then(function(array) {
    console.log("This should be [1, 2, 3]:", array);
});

function fail() {
    return new Promise(function(success, fail) {
        fail(new Error("boom"));
    });
}

all([soon(1), fail(), soon(3)]).then(function(array) {
    console.log("We should not get here");
}, function(error) {
    if (error.message == "boom") console.log("Unexpected failure:", error);
});

// task9
function task9_1(){
    var res1 = document.getElementById("task9");
    alert(res1.getAttribute("class"));
}
function task9_2(){
    var res1 = document.getElementById("task9");
    res1.setAttribute("class", null);
    alert('The class was successfully removed. Check it - push higher button');
}

// task10
function task10_1(){
    var res1 = document.getElementById("task10");
    alert(res1.getAttribute("class"));
}
function task10_2(){
    var res1 = document.getElementById("task10");
    res1.setAttribute("class", "new-class");
    alert('The class was successfully renamed. Check it - push higher button');
}

// task11
function task11(element){
    var res1 = document.getElementById("span_task11");
    res1.innerHTML = element.value;
}

// task12
function task12(){
    var el = document.getElementsByTagName("a");
    for(var i = 0; i < el.length; i++)
    el[i].innerHTML = el[i].innerHTML + ' (' + el[i].getAttribute('href') + ')';
}

// task13
function task13(el){
    el.disabled = true;
    var res = document.getElementById("task13");
    res.innerHTML = 'Another text';
    res.style.color = 'cyan';
}

// task14
function task14(){
    var res = document.getElementsByClassName('par');
    for(var i = 0; i < res.length; i++)
    res[i].innerHTML = i;
}

// task15
function task15(){
    var res = document.getElementsByClassName('par');
    for(var i = 0; i < res.length; i++)
    res[i].innerHTML = i + ' . ' + res[i].innerHTML;
}