// task1
// Сделайте функцию, которая параметрами принимает 2 числа. Если эти числа равны - пусть функция
// вернет true, а если не равны - false
function twoNumbersEqual(x, y) {
  if (x == y) {
    console.log("true");
  } else {
    console.log("false");
  }
}
twoNumbersEqual(4, 2)

// task2
// Сделайте функцию, которая параметрами принимает 2 числа. Если их сумма больше 10 - пусть
// функция вернет true, а если нет - false
function twoNumbersEqual1(x, y) {
    if ((x + y) > 10) {
      console.log("true");
    } else {
      console.log("false");
    }
  }
  twoNumbersEqual1(55, 2)

// task3
// Сделайте функцию, которая параметром принимает число и проверяет - отрицательное оно или нет.
// Если отрицательное - пусть функция вернет true, а если нет - false
function twoNumbersEqual2(y) {
    if (y < 0) {
      console.log("true");
    } else {
      console.log("false");
    }
  }
  twoNumbersEqual2(-5)