// task1
// Если переменная a равна нулю, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 1, 0, -3
var a = -3;
if (a == 0) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task2
// Если переменная a больше нуля, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 1, 0, -3
var a = 0;
if (a > 1) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task3
//  Если переменная a меньше нуля, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 1, 0, -3
var a = 0;
if (a < -3) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task4
//  Если переменная a больше или равна нулю, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 1, 0, -3
var a = 0;
if (a >= 1) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task5
// Если переменная a меньше или равна нулю, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 1, 0, -3
var a = 0;
if (a <= -3) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task6
//  Если переменная a не равна нулю, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 1, 0, -3
var a = 0;
if (a != -3) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task7
//  Если переменная a равна 'test', то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 'test', 'тест', 3
var a = 3;
if (a == "тест") {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task8
//  Если переменная a равна '1' и по значению и по типу, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном '1', 1, 3
var a = 1;
if (a === 3) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task9
// Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите два
// варианта скрипта - с короткой записью и с длинной
var a = true;
if (a == true) {
  console.log("Right");
} else {
  console.log("Wrong");
}

var a = true;
if (a) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task10
// Если переменная test не равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите
// два варианта скрипта - с короткой записью и с длинной
var a = true;
if (a != true) {
  console.log("Right");
} else {
  console.log("Wrong");
}

var a = true;
if (!a) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task11
// Если переменная a больше нуля и меньше 5-ти, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при a, равном 5, 0, -3, 2
var a = 2;
if (a > 0 && a < 5) {
  console.log("Right");
} else {
  console.log("Wrong");
}

// task12
// Если переменная a равна нулю или равна двум, то прибавьте к ней 7, иначе поделите ее на 10. Выведите новое значение переменной на экран.
// Проверьте работу скрипта при a, равном 5, 0, -3, 2
var a = 5;
var r;
if (a == 0 || a == 2) {
  r = a + 7;
} else {
  r = a / 10;
}
console.log(r);

// task13
// Если переменная a равна или меньше 1, а переменная b больше или равна 3, то выведите сумму этих переменных, иначе выведите их разность
// (результат вычитания). Проверьте работу скрипта при a и b, равном 1 и 3, 0 и 6, 3 и 5
var a = 3;
var b = 6;
if (a <= 1 && b >= 5) {
  r = a + b;
} else {
  r = a - b;
}
console.log(r);

// task14
// Если переменная a больше 2-х и меньше 11-ти, или переменная b больше или равна 6-ти и меньше 14-ти, то выведите 'Верно', в противном случае
// выведите 'Неверно'
var a = 3;
var b = 6;
if ((a > 2 && a < 11) || (b >= 6 && b < 14)) {
  r = a + b;
} else {
  r = a - b;
}
console.log(r);

// task15
// Переменная num может принимать 4 значения: 1, 2, 3 или 4. Если она имеет значение '1', то в переменную result запишем 'зима', если имеет значение
// '2' – 'весна' и так далее. Решите задачу через switch-case
var num = 2;
var result;
switch (num) {
  case 1:
    result = "winter";
    break;
  case 2:
    result = "spring";
    break;
  case 3:
    result = "summer";
    break;
  case 4:
    result = "autumn";
    break;
}
console.log(result);

// task16
// В переменной day лежит какое-то число из интервала от 1 до 31. Определите в какую декаду месяца попадает это число (в первую, вторую или третью)
var day = 30;
if (day >= 1 && day <= 10) {
  console.log("first decade");
} else if (day >= 11 && day <= 20) {
  console.log("seconde decade");
} else {
  console.log("third decade");
}

// task17
// В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень)
var month = 12;
var res;
switch (month) {
  case 12:
    res = "winter";
  case 1:
    res = "winter";
  case 2:
    res = "winter";
    break;
  case 3:
    res = "spring";
  case 4:
    res = "spring";
  case 5:
    res = "spring";
    break;
  case 6:
    res = "summer";
  case 7:
    res = "summer";
  case 8:
    res = "summer";
    break;
  case 9:
    res = "autumn";
  case 10:
    res = "autumn";
  case 11:
    res = "autumn";
    break;
}
console.log(res);

// task18
// Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'. Если это так - выведите
// 'да', в противном случае выведите 'нет'
var str = 'abcde';
if (str.charAt(0) == 'a') {
    console.log('Yes');
} else {
    console.log('No');
}

// task19
// Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. Если это так - выведите 'да',
// в противном случае выведите 'нет'
var s_numbers = '12345';
if ((parseInt(s_numbers.charAt(0)) == 1) || (parseInt(s_numbers.charAt(0)) == 2) || (parseInt(s_numbers.charAt(0)) == 3)) {
    console.log('Yes');
} else {
    console.log('No');
}

// task20
// Дана строка из 3-х цифр. Найдите сумму этих цифр. То есть сложите как числа первый символ строки, второй и третий
var example = '496';
var summ = parseInt(example.charAt(0)) + parseInt(example.charAt(1)) + parseInt(example.charAt(2));
console.log(summ);
//alternative
var summ = Number(example[0]) + Number(example[1]) + Number(example[2]);
console.log(summ);

// task21
// Дана строка из 6-ти цифр. Проверьте, что сумма первых трех цифр равняется сумме вторых трех цифр. Если это так - выведите
// 'да', в противном случае выведите 'нет'
var ex = '235990';
var summFirst = parseInt(ex.charAt(0)) + parseInt(ex.charAt(1)) + parseInt(ex.charAt(2));
var summSecond = parseInt(ex.charAt(3)) + parseInt(ex.charAt(4)) + parseInt(ex.charAt(5));
if (summFirst == summSecond) {
    console.log('Yes');
} else {
    console.log('No');
}