// task1
// Дана строка. Сделайте заглавным первый символ каждого слова этой строки. Для этого сделайте 
// вспомогательную функцию ucfirst, которая будет получать строку, делать первый символ этой 
// строки заглавным и возвращать обратно строку с заглавной первой буквой
var str = 'The kids aren\'t ok'
function ucfirst(str){
return str[0].toUpperCase() + str.substr(1)
}
var task1Arr = []
var arrByBS = str.split(' ')
for(var i = 0; i < arrByBS.length; i++){
    task1Arr.push(ucfirst(arrByBS[i]))
}
var otherStr = task1Arr.join(' ')
console.log(task1Arr)

// task2
//  Дана строка вида 'var_text_hello'. Сделайте из него текст 'varTextHello'
var str1 = 'var_text_hello'
function removeDelimeters(str1){

}

// task3
// Сделайте функцию inArray, которая определяет, есть в массиве элемент с заданным текстом или нет.
// Функция первым параметром должна принимать текст элемента, а вторым - массив, в котором делается
// поиск. Функция должна возвращать true или false
function inArray(str, arr) {
	for (var i = 0; i < arr.length; i++) {
		if (arr[i] === str) {
			return true
		}
	}
	return false
}

var str = 'яблоко'
var arr = ['банан', 'яблоко', 'персик']
console.log(inArray(str, arr))

// task4
//  Дана строка, например, '123456'. Сделайте из нее '214365'
var strAn = '123456'
var arr = []
arr = strAn.split('')
console.log(arr)

arr.splice(0, 0, '2')
console.log(arr)
arr.splice(2, 2)
console.log(arr)
arr.splice(3, 2, '3', '6', '5')
console.log(arr)
var rez = arr.join('')
console.log(rez)