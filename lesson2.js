// task1
// Создайте переменную num и присвойте ей значение 3. Выведите значение этой переменной на экран с помощью метода alert
var num = 3;
alert(num);

// task2
// Создайте переменные a=10 и b=2. Выведите на экран их сумму, разность, произведение и частное (результат деления)
var a = 10;
var b = 2;
var res;
res = a + b;
console.log("Sum a and b: " + res);
res = a - b;
console.log("Sub a and b: " + res);
res = a * b;
console.log("Mult a and b: " + res);
res = a / b;
console.log("Div a and b: " + res);

// task3
// Создайте переменные c=15 и d=2. Просуммируйте их, а результат присвойте переменной result. Выведите на экран значение переменной result
var c = 15;
var d = 2;
var result = c + d;
console.log(result);

// task4
// Создайте переменные a=10, b=2 и c=5. Выведите на экран их сумму
var a = 10;
var b = 2;
var c = 5;
var summ = a + b + c;
console.log(summ);

// task5
// Создайте переменные a=17 и b=10. Отнимите от a переменную b и результат присвойте переменной c. Затем создайте переменную d, присвойте ей
// значение 7. Сложите переменные c и d, а результат запишите в переменную result. Выведите на экран значение переменной result.
var a = 17;
var b = 10;
var c = a - b;
var d = 7;
var result = c + d;
console.log(result);

// task6
// Создайте переменную str и присвойте ей значение 'Привет, Мир!'. Выведите значение этой переменной на экран
var str = 'Привет, Мир!';
console.log(str);

// task7
// Создайте переменные str1='Привет, ' и str2='Мир!'. С помощью этих переменных и операции сложения строк выведите на экран фразу 'Привет, Мир!'.
var str1='Привет, ';
var str2='Мир!';
var checkout = str1 + str2;
console.log(checkout);

// task8
// Создайте переменную name и присвойте ей ваше имя. Выведите на экран фразу 'Привет, %Имя%!'
var myName = 'Valera';
console.log('Привет, ' + myName);

// task9
// Создайте переменную age и присвойте ей ваш возраст. Выведите на экран 'Мне %Возраст% лет!'
var myAge = 33;
console.log('Мне ' + myAge + ' года!');

// task10
// Спросите имя пользователя с помощью методы prompt. Выведите с помощью alert сообщение 'Ваше имя %имя%'
var ask = prompt('What\'s your name?');
alert('Ваше имя ' + ask);

// task11
// Спросите у пользователя число. Выведите с помощью alert квадрат этого числа
var ask = prompt('Enter some number');
alert('Square of this number is: ' + ask * ask);

// task12
// Создайте переменную str и присвойте ей значение 'abcde'. Обращаясь к отдельным символам этой строки выведите
// на экран символ 'a', символ 'c', символ 'e'
var str = 'abcde';
console.log("Character " + str.substr(0, 1));
console.log("Character " + str.substr(2, 1));
console.log("Character " + str.substr(4, 1));

// task13
// Создайте переменную num и присвойте ей значение '12345'. Найдите произведение (умножение) цифр этого числа
var num = '12345';
var mult = num.substr(0, 1) * num.substr(1, 1) * num.substr(2, 1) * num.substr(3, 1) * num.substr(4, 1);
console.log("Multiply of 5 numbers are: " + mult);

// task14
// Напишите скрипт, который считает количество секунд в часе, в сутках, в месяце
var hour = 60 * 60;
var day = hour * 24;
var month = day * 30;
console.log("Seconds in hour: " + hour + "; in day: " + day + "; in month " + month);

// task15
// Создайте три переменные - час, минута, секунда. С их помощью выведите текущее время в формате 'час:минута:секунда'
var hour;
var minute;
var second;
var date = new Date();
var current = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
console.log(current);

// task16
// Создайте переменную, присвойте ей число. Возведите это число в квадрат. Выведите его на экран
// duplicate  task11

// task17
// Переделайте этот код так, чтобы в нем использовались операции +=, -=, *=, /=. Количество строк кода
// при этом не должно измениться
// var num = 47;
// num = num + 7;
// num = num - 18;
// num = num * 10;
// num = num / 15;
// alert(num);
var num = 47;
num += 7;
num -= 18;
num *= 10;
num /= 15;
alert(num);

// task18
// Переделайте этот код так, чтобы в нем использовались операции ++ и --. Количество строк кода при этом не должно измениться
// var num = 10;
// num = num + 1;
// num = num + 1;
// num = num - 1;
// alert(num);
var num = 10;
num++;
num++;
num--;
alert(num);