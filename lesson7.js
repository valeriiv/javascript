// task1
// Дана строка 'js'. Сделайте из нее строку 'JS'
let str = "js";
console.log(str.toLocaleUpperCase());

// task2
// Дана строка 'JS'. Сделайте из нее строку 'js'
let str1 = "JS";
console.log(str1.toLocaleLowerCase());

// task3
// Дана строка 'я учу javascript!'. Найдите количество символов в этой строке
let str2 = 'я учу javascript!';
console.log(str2.length);

// task4
// Дана строка 'я учу javascript!'. Вырежите из нее слово 'учу' и слово 'javascript' тремя разными способами (через substr, substring, slice)
let str3 = 'я учу javascript!';
console.log(str3.substr(2, 3));
console.log(str3.substring(2, 5));
console.log(str3.slice(2, 5));

// task5
// Дана строка 'я учу javascript!'. Найдите позицию подстроки 'учу'
let str4 = 'я учу javascript!';
console.log(str4.indexOf("учу"));

// task6
// Дана переменная str, в которой хранится какой-либо текст. Реализуйте обрезание длинного текста по следующему принципу: если количество символов
// этого текста больше заданного в переменной n, то в переменную result запишем первые n символов строки str и добавим в конец троеточие '...'. В
// противном случае в переменную result запишем содержимое переменной str.
let str5 = "Дана переменная str, в которой хранится какой-либо текст. Реализуйте обрезание длинного текста по следующему принципу: если количество символов";
let n = Math.ceil(Math.random()*20);
let result;
if (str5.length > n) {
    result = str5.substr(0, n) + "...";
} else {
    result = str5;
}
console.log(result);
console.log(n);

// task7
// Дана строка 'Я-учу-javascript!'. Замените все дефисы на '!' с помощью глобального поиска и замены
let str6 = 'Я-учу-javascript!';
console.log(str6.replace(/-/g, '!'));

// task8
// Дана строка 'я учу javascript!'. С помощью метода split запишите каждое слово этой строки в отдельный элемент массива
let str7 = 'я учу javascript!';
let arr = [];
arr = str7.split(' ');
console.log(arr);

// task9
// Дана строка 'я учу javascript!'. С помощью метода split запишите каждый символ этой строки в отдельный элемент массива. 
let str8 = 'я учу javascript!';
let arr1 = [];
arr1 = str8.split("");
console.log(arr1);

// task10
// В переменной date лежит дата в формате '2025-12-31'. Преобразуйте эту дату в формат '31.12.2025'
var d = '2025-12-31';
var new_d = d.split('-');
var arr2 = new_d[2] + "." + new_d[1] + "." + new_d[0];
console.log(arr2);

// task11
// Дан массив ['я', 'учу', 'javascript', '!']. С помощью метода join преобразуйте массив в строку 'я+учу+javascript+!'
var arr3 = ['я', 'учу', 'javascript', '!'];
console.log(arr3.join('+'));

// task12
// Преобразуйте первую букву строки в верхний регистр
let str9 = 'я учу javascript!';
let one = str9.substr(0, 1).toUpperCase() + str9.substr(1);
console.log(one);

// task13
// Преобразуйте первую букву каждого слова строки в верхний регистр
var str10 = 'я учу javascript!';
var two = str10.split(' ');

for(var y = 0; y < two.length; y++) {
two[y] = two[y].slice(0, 1).toUpperCase() + two[y].slice(1);
}
console.log(two);

// task14
// Преобразуйте строку 'var_test_text' в 'varTestText'. Скрипт, конечно же, должен работать с любыми аналогичными строками
var testStr = 'var_test_text';
var three = testStr.split('_');

for(var y = 1; y < three.length; y++) {
three[y] = three[y].slice(0, 1).toUpperCase() + three[y].slice(1);
}
var res = three.join('');
console.log(res);