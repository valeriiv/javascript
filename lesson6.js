// task1
// Даны переменные a = 10 и b = 3. Найдите остаток от деления a на b
var a = 10;
var b = 3;
var res = a % b;
console.log(res);

// task2
// Даны переменные a и b. Проверьте, что a делится без остатка на b. 
// Если это так - выведите 'Делится' и результат деления, иначе выведите 'Делится с остатком' и остаток от деления
var a = 31;
var b = 14;
var result = (a % b);
if (result == 0) {
    console.log('Division without reminder');
} else {
    console.log('Remainder of the division = ' + result);
}

// task3
// Возведите 2 в 10 степень. Результат запишите в переменную st
var a = 2;
var b = 10;
var st = Math.pow(2, 10);
console.log('Возведите 2 в 10 степень = ' + st);

//task4
// Найдите квадратный корень из 245
var x = 245;
var res = Math.sqrt(x);
console.log(res);

// task5
// Дан массив с элементами 4, 2, 5, 19, 13, 0, 10. Найдите квадратный корень из суммы кубов его элементов.
// Для решения воспользуйтесь циклом for
var arr = [4, 2, 5, 19, 13, 0, 10];
var res = 0;;
var resf = 0;
for(var i = 0; i < arr.length; i++){
    res+=arr[i];
}
resf = (Math.sqrt(res)).toFixed(4);
console.log(resf);

// task6
// Найдите квадратный корень из 379. Результат округлите до целых, до десятых, до сотых
var x = 379;
var resInteger = (Math.sqrt(x)).toFixed(0);
console.log(resInteger);
var resOneTenth = (Math.sqrt(x)).toFixed(1);
console.log(resOneTenth);
var resOneHundredth = (Math.sqrt(x)).toFixed(2);
console.log(resOneHundredth);

// task7
// Найдите квадратный корень из 587. Округлите результат в большую и меньшую стороны, запишите
// результаты округления в объект с ключами 'floor' и 'ceil'
var x = 587;
var resFl = Math.floor(Math.sqrt(x));
var resCe = Math.ceil(Math.sqrt(x));
var obj = {'floor':resFl, 'ceil':resCe};
console.log(obj);

// task8
// Даны числа 4, -2, 5, 19, -130, 0, 10. Найдите минимальное и максимальное число
console.log('Min is: ' + Math.min(4, -2, 5, 19, -130, 0, 10));
console.log('Max is: ' + Math.max(4, -2, 5, 19, -130, 0, 10));

// task9
// Выведите на экран случайное целое число от 1 до 100
var num = 100;
console.log(Math.ceil(Math.random() * num));

// task10
// Заполните массив 10-ю случайными целыми числами
var arrA = new Array(10);
var exampleN = 100;
for(var i = 0; i < arrA.length; i++) arrA[i] = Math.ceil(Math.random() * exampleN);
console.log(arrA);

// task11
// Даны переменные a и b. Найдите найдите модуль разности a и b. Проверьте работу скрипта
// самостоятельно для различных a и b
var a = -50;
var b = -46;
var res = Math.abs(a - b);
console.log(res);

// task12
// Даны переменные a и b. Отнимите от a переменную b и результат присвойте переменной c. Сделайте
// так, чтобы в любом случае в переменную c записалось положительное значение. Проверьте работу скрипта
// при a и b, равных соответственно 3 и 5, 6 и 1
var a = 6;
var b = 1;
var c = Math.abs(a - b);
console.log(c);

// task13
// Дан массив arr. Найдите среднее арифметическое его элементов. Проверьте задачу на массиве с элементами
// 12, 15, 20, 25, 59, 79
var arr = [12, 15, 20, 25, 59, 79, 4];
var result = 0;
var counterOfElement = 0;
var finalResult = 0;
for(var i = 0; i < arr.length; i++) {
result+=arr[i];
counterOfElement++;
}
finalResult = result / counterOfElement;
console.log(finalResult);

// task14
// Напишите скрипт, который будет находить факториал числа. Факториал (обозначается !) - это произведение
// (умножение) всех целых чисел, меньше данного, и его самого. Например, 4! = 1*2*3*4
function fact(number){
    if (number == 1) return 1;
    if (number == 2) return 2;
    return number * fact(number - 1);
}
console.log(fact(5));