// task1
// Сделайте функцию, которая возвращает квадрат числа. Число передается параметром
function myPow(x){
    return x * x
}
console.log(myPow(3))

// task2
// Сделайте функцию, которая возвращает сумму двух чисел
function sum(x, y){
    return x + y
}
console.log(sum(13, -3))

// task3
// Сделайте функцию, которая отнимает от первого числа второе и делит на третье
function threeNumbers(){
    return (arguments[0] - arguments[1]) / arguments[2]
}
console.log(threeNumbers(13, 4, 6))

//OR use explicit designation
function threeNumbers(x, y, z){
    return (x - y) / z
}
console.log(threeNumbers(13, 4, 6))

// task4
// Сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день
// недели на русском языке
function dayOfTheWeekRu(x){
    var sevenDays = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье']
    if (x > 7) {
        x = 7
    } 
    if(x < 1) {
        x = 1
    }
    switch(x){
        case 1: console.log(sevenDays[0]);
        break;
        case 2: console.log(sevenDays[1]);
        break;
        case 3: console.log(sevenDays[2]);
        break;
        case 4: console.log(sevenDays[3]);
        break;
        case 5: console.log(sevenDays[4]);
        break;
        case 6: console.log(sevenDays[5]);
        break;
        case 7: console.log(sevenDays[6]);
        break;
    }
}
dayOfTheWeekRu(1);
dayOfTheWeekRu(-1);
dayOfTheWeekRu(5);
dayOfTheWeekRu(8);